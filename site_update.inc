<?php

// Version of dump file, not necessarily same as code version.
define('SITE_UPDATE_FORMAT_VERSION', '7.3'); // Use string to avoid floating point nonsense.

// Use $conf, not variable_get(), to be included by bin/site_update_dump.
global $conf;

if (!empty($conf['site_update_is_base'])) {
  // Defaults for live/working copies.
  $site_update_defaults = array(
    'site_update_id_bump' => 0,
    'site_update_id_offset' => 1,
    'site_update_id_increment' => 10,
    'site_update_php_file' => 'sites/all/database/site_update.dump',
  );
}
else {
  // Defaults for base.
  $site_update_defaults = array(
    'site_update_id_bump' => 0,
    'site_update_id_offset' => 10,
    'site_update_id_increment' => 10,
    'site_update_php_file' => 'sites/all/database/site_update.dump',
  );
}

// Define SITE_UPDATE_PHP_FILE and other constants.
foreach ($site_update_defaults as $key => $value) {
  if (!isset($conf[$key])) {
    define(strtoupper($key), $value);
  }
  else {
    define(strtoupper($key), $conf[$key]);
  }
}
