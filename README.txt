== Site Update for Drupal 7.x ==

The site_update module is for migrating configuration changes from one
drupal installation to another. And for keeping multiple copies of a
site in sync. Why would you have multiple copies of a site?  When you
have one for developement, another for staging, and one live. Or when
you have multiple developers each working on their own development
copy.


== Creating the Base Site ==

To use this module, you must create one copy of the site which is the
"base". The base should have little or no actual end-user content, but
only settings and content that each and every copy of the site must
have. So for example, the base might have a node which is the site's
About page, but it shouldn't have lots of forum posts.  The base might
have taxonomy terms that control privacy or other behaviors, but
shouldn't have freetagging terms that end users contribute.

To set up the base site:

1. add $conf['site_update_is_base'] = TRUE; to the bottom of the
settings.php file (in a standard install, that is
sites/default/settings.php)

2. Configure the database to have an auto_increment_increment (default
10).  One way to do this, for mysql, is a server-wide setting in my.conf.
Perhaps an easier way for Drupal users is again in settings.php, by adding
something like:

// Site_update.module settings.
$conf['site_update_is_base'] = TRUE; // Set to TRUE only in the base site's settings.php.
$conf['site_update_id_bump'] = 1000001; // Optional, could be 0.
// Values of 1 and 10 make ID sequences 1, 11, 21, 31, etc. (recommended for base)
// Values of 10 and 10 make ID sequences 10, 20, 30, etc. (recommended for live)
$conf['site_update_id_offset'] = 1;
$conf['site_update_id_increment'] = 10;

// Setup mysql auto_increments for use with site_update.module.
if (!empty($databases) && empty($databases['default']['default']['init_commands']) &&
    !empty($conf['site_update_id_offset']) && !empty($conf['site_update_id_increment'])) {
  $databases['default']['default']['init_commands'] = array(
    'SET auto_increment_offset  = ' . $conf['site_update_id_offset'],
    'SET auto_increment_increment  = ' . $conf['site_update_id_increment'],
  );
}

Note that for best results, you should install your base site with
your database configured as described above.  That is, have this
configuration when install.php is run.  Drupal makes this tricky, as
the settings have to be placed in both your settings.php AND
sites/defaults/default.settings.php.

If you installed your base site without these settings, the initial
items written to the database will not have the expected offset and
increment.  To compensate for this, you must set the
site_update_id_bump to be higher than any ID in your base database.
You could for example set $conf['site_update_id_bump'] = 1000001; Note
that value must be set for every copy of your site.  Base, live and
all development copies must have the same value for
site_update_id_bump.


To set up a working copy or live copy of a site:

1. Configure the database to have an auto_increment_increment
identical to the base, but a different auto_increment_offset.  One way
to do this is a server-wide setting in my.conf.  (You'll need a
different instance of mysql for the base and for live!)  Alternatively
you could configure this in settings.php, by adding something like the
following.

// Site_update.module settings.
$conf['site_update_is_base'] = TRUE; // Set to TRUE only in the base site's settings.php.
$conf['site_update_id_bump'] = 1000001; // Must be the same for all copies of site.
// Values of 1 and 10 make ID sequences 1, 11, 21, 31, etc. (recommended for base)
// Values of 10 and 10 make ID sequences 10, 20, 30, etc. (recommended for live)
$conf['site_update_id_offset'] = 10; // Must be different from base copy.
$conf['site_update_id_increment'] = 10; // Must be the same for all copies of site.

// Setup mysql auto_increments for use with site_update.module.
if (!empty($databases) && empty($databases['default']['default']['init_commands']) &&
    !empty($conf['site_update_id_offset']) && !empty($conf['site_update_id_increment'])) {
  $databases['default']['default']['init_commands'] = array(
    'SET auto_increment_offset  = ' . $conf['site_update_id_offset'],
    'SET auto_increment_increment  = ' . $conf['site_update_id_increment'],
  );
}

Again, you'll want these settings not just in you settings.php, but
also in default.settings.php when you install the working copy of your
site.  Unfortunately Drupal replaces settings.php with the contents of
default.settings.php halfway through the install process, so care must
be taken to get this right!

== How to Use Site Update After Installation ==

Next, use Drupal's admin pages to configure your base site the way you
want it.  You can change variables, install modules, add content, or
anything else that configures Drupal's database.  When the base has
the settings you want, create a "dump" of the database.  You'll see a
link to do this from the status page.

Once the dump is made, you can commit
sites/all/database/site_update.dump into source code control. Use this to
replicate the base system on every copy of the site. When settings are
changed on the base, re-run the dump script and commit the changed
site_update.dump.

Make sure each site copy has site_update.module enabled. One way to do
that is via an install profile.

== Updating Existing Sites ==

On each copy of the site, go to Reports | Status Report
(/admin/reports/status) and click the Site Update link. When complete,
the copy should include configuration changes made to the base.

Note the site_update dump will not overwrite existing data by default.
It will only add new rows. Typically, this is the desired behavior
for some tables, but not all. To customize this specifically for your
site, build a custom module and implement hook_site_update in your
module's install file. Read that again...implement this hook in
custom.install (not custom.module as you normally would). Here's an
example of what it might look like.

Optionally, if you want to refresh some part of the data just once,
and not every time, you may call site_update_refresh_______() from an
instance of hook_update_N, instead of hook_site_update().

/**
 * This hook tells site_update to overwrite some settings. That is, give the base priority over the working copy for some data.
 *
 * See site_update.install for all the refresh options.
 */
function custom_site_update($op) {
  if ($op == 'post_system') {
    site_update_refresh_system();
    site_update_refresh_node_types();
    site_update_refresh_permissions();
    site_update_refresh_input_formats();

    site_update_refresh_themes();

    // Some blocks defined in base.
    site_update_refresh_blocks();

    // Some views defined in base
    //db_query('DELETE FROM {views_display} WHERE vid=5');


    // Recent changes to story form and more...
    //site_update_refresh_profile_fields();

    //site_update_refresh_taxonomy();

    $vars = array(
      //'javascript_parsed',
      'user_register',
      'i18ntaxonomy_vocabulary',
      'profile_block_author_fields',
    );
    site_update_refresh_variables($vars);

  }
}
